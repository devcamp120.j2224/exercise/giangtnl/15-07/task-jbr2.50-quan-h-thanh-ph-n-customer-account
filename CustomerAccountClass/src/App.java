public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("JBR2.50");

        //Khởi tạo đối tượng customer 1
        Customer customer1 = new Customer(1, "Giang", 20);
        //Khởi tạo đối tượng customer 2
        Customer customer2 = new Customer(2, "Linh", 10);

        System.out.println(customer1);
        System.out.println(customer2);

        //Khởi tạo đối tượng account 1
        Account account1 = new Account(1, customer1,5500);
        Account account2 = new Account(2, customer2,9500);

        System.out.println(account1);
        System.out.println(account2);
        
    }
}
